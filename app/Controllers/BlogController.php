<?php

namespace App\Controllers;
use App\models\Model;

class BlogController extends Controller{

    public function index()
    {

        return $this->view('index');
    }

    public function login($login="login",$password="password")
    {
        session_start();
        $_SESSION['user']=false;     
        
        $user = new Model;
        $check = $user->user_logged($login,$password);

        if($check){
            $_SESSION['user']=true;
            echo "Your are logged";
        }else{
            echo "Wrong password or login";
        } 

    }

    public function loginView()
    {
        return $this->view('login');
    }

    public function articles(){
        $data = new Model; 
        $data = $data->dataBlog();

        return $this->view('index',compact('data'));
    }

}   